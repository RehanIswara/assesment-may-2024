CREATE TABLE "accounts" (
  "id" bigserial,
  "customer_id" bigint NOT NULL,
  "no_rekening" varchar PRIMARY KEY,
  "saldo" bigint NOT NULL
  );

CREATE TABLE "entries" (
  "id" bigserial PRIMARY KEY,
  "kode_transaksi" varchar(1) NOT NULL,
  "waktu" timestamptz NOT NULL DEFAULT (now()),
  "nominal" bigint NOT NULL,
  "no_rekening" varchar NOT NULL
);

CREATE TABLE "mutasi" (
  "id" bigserial PRIMARY KEY,
  "tanggal_transaksi" timestamptz NOT NULL DEFAULT (now()),
  "no_rekening" varchar,
  "jenis_transaksi" varchar,
  "nominal" bigint
);

COMMENT ON COLUMN "entries"."nominal" IS 'can be negative or positive';

ALTER TABLE "accounts" ADD FOREIGN KEY ("customer_id") REFERENCES "customers" ("id");
ALTER TABLE "entries" ADD FOREIGN KEY ("no_rekening") REFERENCES "accounts" ("no_rekening");
ALTER TABLE "mutasi" ADD FOREIGN KEY ("no_rekening") REFERENCES "accounts" ("no_rekening");