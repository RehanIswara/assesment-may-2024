-- name: CreateMutasi :one
INSERT INTO mutasi (
    tanggal_transaksi,
    jenis_transaksi,
    no_rekening,
    nominal
) VALUES (
    $1, $2, $3, $4
) RETURNING *;