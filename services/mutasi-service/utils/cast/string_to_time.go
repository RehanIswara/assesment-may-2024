package cast

import (
	"time"
)

func StringToTime(layout, timeString string) (time.Time, error) {
	parsedTime, err := time.Parse(layout, timeString)
	if err != nil {
		return time.Time{}, err
	}

	return parsedTime, nil
}
