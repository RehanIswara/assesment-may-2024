package service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"time"

	"mutasi-service/store/postgres_store/sqlc"
	"mutasi-service/utils/cast"
	"mutasi-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type CreateMutasiParams struct {
	TanggalTransaksi time.Time `json:"tanggal_transaksi"`
	NoRekening       string    `json:"no_rekening"`
	Nominal          int64     `json:"nominal"`
	JenisTransaksi   string    `json:"jenis_transaksi"`
}

type Mutasi struct {
	ID               int64     `json:"id"`
	TanggalTransaksi time.Time `json:"tanggal_transaksi"`
	NoRekening       string    `json:"no_rekening"`
	Nominal          int64     `json:"nominal"`
}

type CreateMutasiResult struct {
	Mutasi Mutasi `json:"saldo"`
}

func (service *Service) CreateMutasi(ctx context.Context, params *CreateMutasiParams) (*CreateMutasiResult, error) {
	const op errs.Op = "service/CreateMutasi"

	serviceResult := &CreateMutasiResult{}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!")

	mutasi, err := service.store.postgres.CreateMutasi(ctx, sqlc.CreateMutasiParams{
		TanggalTransaksi: params.TanggalTransaksi,
		NoRekening:       sql.NullString{Valid: true, String: params.NoRekening},
		Nominal:          sql.NullInt64{Valid: true, Int64: params.Nominal},
		JenisTransaksi:   sql.NullString{Valid: true, String: params.JenisTransaksi},
	})
	if err != nil {
		return nil, err
	}

	serviceResult.Mutasi = func(mutasi sqlc.Mutasi) Mutasi {
		mappedMutasi := Mutasi{
			ID:               mutasi.ID,
			TanggalTransaksi: mutasi.TanggalTransaksi,
			NoRekening:       mutasi.NoRekening.String,
			Nominal:          mutasi.Nominal.Int64,
		}

		return mappedMutasi
	}(mutasi)

	return serviceResult, nil
}

func (service *Service) NewCreateMutasiParamsFromMap(mapParams map[string]interface{}) (*CreateMutasiParams, error) {
	const op errs.Op = "service/NewCreateMutasiParamsFromMap"

	params, err := func(mapParams map[string]interface{}) (*CreateMutasiParams, error) {
		params := &CreateMutasiParams{}

		tanggalTransaksiString, ok := mapParams["tanggal_transaksi"].(string)
		if !ok {
			return nil, fmt.Errorf("`tanggal_transaksi` as string, got type %T", mapParams["tanggal_transaksi"])
		}
		if len(tanggalTransaksiString) == 0 {
			return nil, errors.New("invalid `tanggal_transaksi`")
		}
		tanggalTransaksi, err := cast.StringToTime("20060102150405", tanggalTransaksiString)
		if err != nil {
			return nil, fmt.Errorf("failed to cast `tanggal_transaksi` as Time: %s", err.Error())
		}
		params.TanggalTransaksi = tanggalTransaksi

		jenisTransaksi, ok := mapParams["jenis_transaksi"].(string)
		if !ok {
			return nil, fmt.Errorf("`jenis_transaksi` as string, got type %T", mapParams["jenis_transaksi"])
		}
		params.JenisTransaksi = jenisTransaksi

		noRekening, ok := mapParams["no_rekening"].(string)
		if !ok {
			return nil, fmt.Errorf("`no_rekening` as string, got type %T", mapParams["no_rekening"])
		}
		params.NoRekening = noRekening

		nominalString, ok := mapParams["nominal"].(string)
		if !ok {
			return nil, fmt.Errorf("`nominal` as string, got type %T", mapParams["nominal"])
		}
		nominal, err := strconv.ParseInt(nominalString, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("failed to cast `nominal` as int64: %s", err.Error())
		}
		params.Nominal = int64(nominal)

		return params, nil
	}(mapParams)
	if err != nil {
		e := fmt.Errorf("failed to extract value: %s", err.Error())

		service.logger.WithFields(logrus.Fields{
			"op":    op,
			"scope": "Data Extraction",
			"err":   e.Error(),
		}).Error("error!")

		return nil, e
	}

	return params, nil
}
