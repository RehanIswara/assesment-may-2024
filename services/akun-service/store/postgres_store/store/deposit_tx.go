package postgres_store

import (
	"context"
	"database/sql"
	"errors"

	"akun-service/store/postgres_store/sqlc"
	"akun-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type DepositTxParams struct {
	Nominal    int64  `json:"nominal"`
	NoRekening string `json:"no_rekening"`
}

type DepositTxResult struct {
	Account sqlc.Account `json:"account"`
	Entry   sqlc.Entry   `json:"entry"`
}

func (store *PostgresStore) DepositTx(ctx context.Context, arg DepositTxParams) (DepositTxResult, error) {
	const op errs.Op = "postgres_store/DepositTx"

	var result DepositTxResult

	err := store.execTx(ctx, func(q *sqlc.Queries) error {
		var err error

		store.logger.WithFields(logrus.Fields{
			"op":  op,
			"arg": arg,
		}).Debug("arg!")

		// get account
		account, err := q.GetAccount(ctx, arg.NoRekening)
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "GetAccount",
				"err":   err.Error(),
			}).Error("error!")

			if err == sql.ErrNoRows {
				return errors.New("nomor rekening tidak dikenali")
			}

			return err
		}

		store.logger.WithFields(logrus.Fields{
			"op":  op,
			"arg": arg,
		}).Debug("arg!!")

		// update saldo
		result.Account, err = q.UpdateSaldo(ctx, sqlc.UpdateSaldoParams{
			NoRekening: arg.NoRekening,
			Saldo:      account.Saldo + arg.Nominal,
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "UpdateSaldo",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		store.logger.WithFields(logrus.Fields{
			"op":  op,
			"arg": arg,
		}).Debug("arg!!!")

		// create entry
		result.Entry, err = q.CreateEntry(ctx, sqlc.CreateEntryParams{
			KodeTransaksi: "C",
			Nominal:       arg.Nominal,
			NoRekening:    arg.NoRekening,
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "CreateEntry",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		store.logger.WithFields(logrus.Fields{
			"op":  op,
			"arg": arg,
		}).Debug("arg!!!!")

		return err
	})

	return result, err
}
