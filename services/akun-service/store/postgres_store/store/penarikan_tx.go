package postgres_store

import (
	"context"

	"akun-service/store/postgres_store/sqlc"
	"akun-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type PenarikanTxParams struct {
	Nominal    int64  `json:"nominal"`
	NoRekening string `json:"no_rekening"`
}

type PenarikanTxResult struct {
	Account sqlc.Account `json:"account"`
	Entry   sqlc.Entry   `json:"entry"`
}

func (store *PostgresStore) PenarikanTx(ctx context.Context, arg PenarikanTxParams) (PenarikanTxResult, error) {
	const op errs.Op = "postgres_store/PenarikanTx"

	var result PenarikanTxResult

	err := store.execTx(ctx, func(q *sqlc.Queries) error {
		var err error

		// get account
		account, err := q.GetAccount(ctx, arg.NoRekening)
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "GetAccount",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		// update saldo
		result.Account, err = q.UpdateSaldo(ctx, sqlc.UpdateSaldoParams{
			NoRekening: arg.NoRekening,
			Saldo:      account.Saldo - arg.Nominal,
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "UpdateSaldo",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		// create entry
		result.Entry, err = q.CreateEntry(ctx, sqlc.CreateEntryParams{
			KodeTransaksi: "D",
			Nominal:       -arg.Nominal,
			NoRekening:    arg.NoRekening,
		})
		if err != nil {
			store.logger.WithFields(logrus.Fields{
				"op":    op,
				"scope": "CreateEntry",
				"err":   err.Error(),
			}).Error("error!")

			return err
		}

		return err
	})

	return result, err
}
