// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.25.0

package sqlc

import (
	"database/sql"
	"time"
)

type Account struct {
	ID         sql.NullInt64 `json:"id"`
	CustomerID int64         `json:"customer_id"`
	NoRekening string        `json:"no_rekening"`
	Saldo      int64         `json:"saldo"`
}

type Customer struct {
	ID   int64  `json:"id"`
	Nama string `json:"nama"`
	Nik  string `json:"nik"`
	NoHp string `json:"no_hp"`
	Pin  string `json:"pin"`
}

type Entry struct {
	ID            int64     `json:"id"`
	KodeTransaksi string    `json:"kode_transaksi"`
	Waktu         time.Time `json:"waktu"`
	// can be negative or positive
	Nominal    int64  `json:"nominal"`
	NoRekening string `json:"no_rekening"`
}

type Mutasi struct {
	ID               int64          `json:"id"`
	TanggalTransaksi time.Time      `json:"tanggal_transaksi"`
	JenisTransaksi   sql.NullString `json:"jenis_transaksi"`
	NoRekening       sql.NullString `json:"no_rekening"`
	Nominal          sql.NullInt64  `json:"nominal"`
}
