-- name: CreateCustomer :one
INSERT INTO customers (
    nama,
    nik,
    no_hp,
    pin
) VALUES (
    $1, $2, $3, $4
) RETURNING *;

-- name: GetCustomer :one
SELECT * FROM customers
WHERE id = $1;