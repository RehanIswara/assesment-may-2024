package akun_service

import (
	"context"
	"time"

	postgres_store "akun-service/store/postgres_store/store"
	"akun-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type DepositParams struct {
	Nominal    int64  `json:"nominal"`
	NoRekening string `json:"no_rekening"`
}

type DepositResult struct {
	Saldo int64 `json:"saldo"`
}

func (service *Service) Deposit(ctx context.Context, params *DepositParams) (*DepositResult, error) {
	const op errs.Op = "akun_service/Deposit"

	serviceResult := &DepositResult{}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!")

	storeResult, err := service.store.postgres.DepositTx(ctx, postgres_store.DepositTxParams{
		Nominal:    params.Nominal,
		NoRekening: params.NoRekening,
	})
	if err != nil {
		return nil, err
	}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!!")

	err = service.store.redis.AddToStream(ctx, service.config.RedisMutasiRequestStream, map[string]interface{}{
		"tanggal_transaksi": time.Now().Format("20060102150405"),
		"no_rekening":       params.NoRekening,
		"nominal":           params.Nominal,
		"jenis_transaksi":   "D",
	})
	if err != nil {
		return nil, err
	}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!!!")

	serviceResult.Saldo = storeResult.Account.Saldo

	return serviceResult, nil
}
