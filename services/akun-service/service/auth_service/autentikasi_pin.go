package auth_service

import (
	"context"
	"database/sql"
	"errors"

	"akun-service/utils/errs"

	"github.com/sirupsen/logrus"
)

type AutentikasiPinParams struct {
	NoRekening string `json:"no_rekening"`
	Pin        string `json:"pin"`
}

type AutentikasiPinResult struct {
	Authenticated bool `json:"authenticated"`
}

func (service *Service) AutentikasiPin(ctx context.Context, params *AutentikasiPinParams) (*AutentikasiPinResult, error) {
	const op errs.Op = "auth_service/AutentikasiPin"

	serviceResult := &AutentikasiPinResult{}

	service.logger.WithFields(logrus.Fields{
		"op":     op,
		"params": params,
	}).Debug("params!")

	account, err := service.store.postgres.GetAccount(ctx, params.NoRekening)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("nomor rekening tidak dikenali")
		}

		return nil, err
	}

	customer, err := service.store.postgres.GetCustomer(ctx, account.CustomerID)
	if err != nil {
		return nil, err
	}

	serviceResult.Authenticated = params.Pin == customer.Pin

	return serviceResult, nil
}
