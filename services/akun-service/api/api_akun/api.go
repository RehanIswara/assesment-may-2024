package api_akun

import (
	"akun-service/service/akun_service"
)

type Api struct {
	service *akun_service.Service
}

func NewApi(akunService *akun_service.Service) *Api {
	return &Api{
		service: akunService,
	}
}
