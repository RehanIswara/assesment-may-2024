package api_auth

import (
	"context"

	"akun-service/service/akun_service"
	"akun-service/service/auth_service"

	"github.com/gofiber/fiber/v2"
)

func (api *Api) Deposit(c *fiber.Ctx) error {
	pin := c.Get("Authorization")
	if pin == "" {
		return c.Status(fiber.StatusUnauthorized).JSON(map[string]interface{}{
			"remark": "`Authorization` header is missing",
		})
	}

	var params *akun_service.DepositParams
	if err := c.BodyParser(&params); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(map[string]interface{}{
			"remark": "failed to parse request body",
		})
	}

	authParams := &auth_service.AutentikasiPinParams{
		NoRekening: params.NoRekening,
		Pin:        pin,
	}

	// call service layer
	result, err := api.service.AutentikasiPin(context.Background(), authParams)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(map[string]interface{}{
			"remark": err.Error(),
		})
	}

	if !result.Authenticated {
		return c.Status(fiber.StatusUnauthorized).JSON(map[string]interface{}{
			"remark": "invalid pin",
		})
	}

	return c.Next()
}
