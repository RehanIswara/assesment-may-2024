CREATE DATABASE banking_isi;

\c banking_isi;

CREATE TABLE "customers" (
  "id" bigserial PRIMARY KEY,
  "nama" varchar NOT NULL,
  "nik" varchar UNIQUE NOT NULL,
  "no_hp" varchar UNIQUE NOT NULL,
  "pin" varchar NOT NULL
);

INSERT INTO "customers" (nama,nik,no_hp,pin) VALUES ('John', '112233', '1234', '123');

CREATE TABLE "accounts" (
  "id" bigserial,
  "customer_id" bigint NOT NULL,
  "no_rekening" varchar PRIMARY KEY,
  "saldo" bigint NOT NULL
);

INSERT INTO "accounts" (customer_id, no_rekening, saldo) VALUES (1, '1313', 1000000);

CREATE TABLE "entries" (
  "id" bigserial PRIMARY KEY,
  "kode_transaksi" varchar(1) NOT NULL,
  "waktu" timestamptz NOT NULL DEFAULT (now()),
  "nominal" bigint NOT NULL,
  "no_rekening" varchar NOT NULL
);

CREATE TABLE "mutasi" (
  "id" bigserial PRIMARY KEY,
  "tanggal_transaksi" timestamptz NOT NULL DEFAULT (now()),
  "no_rekening" varchar,
  "nominal" bigint,
  "jenis_transaksi" varchar
);

COMMENT ON COLUMN "entries"."nominal" IS 'can be negative or positive';

ALTER TABLE "accounts" ADD FOREIGN KEY ("customer_id") REFERENCES "customers" ("id");
ALTER TABLE "entries" ADD FOREIGN KEY ("no_rekening") REFERENCES "accounts" ("no_rekening");